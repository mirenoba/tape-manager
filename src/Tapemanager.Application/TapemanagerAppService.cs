﻿using System;
using System.Collections.Generic;
using System.Text;
using Tapemanager.Localization;
using Volo.Abp.Application.Services;

namespace Tapemanager
{
    /* Inherit your application services from this class.
     */
    public abstract class TapemanagerAppService : ApplicationService
    {
        protected TapemanagerAppService()
        {
            LocalizationResource = typeof(TapemanagerResource);
        }
    }
}
