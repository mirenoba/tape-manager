﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Tapemanager.Permissions;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace Tapemanager.Countries
{
    [Authorize(TapemanagerPermissions.Countries.Default)]
    public class CountryAppService : AsyncCrudAppService<Country, CountryDto, Guid, CountryListRequestDto, CountryDto, UpdateCountryDto>, ICountryAppService
    {
        #region Permission Names
        protected override string GetPolicyName => TapemanagerPermissions.Countries.List;
        protected override string GetListPolicyName => TapemanagerPermissions.Countries.List;
        protected override string UpdatePolicyName => TapemanagerPermissions.Countries.Update;
        #endregion

        public CountryAppService(IRepository<Country, Guid> repository) : base(repository)
        {
        }

        [AllowAnonymous]
        public override Task<CountryDto> GetAsync(Guid id)
        {
            return base.GetAsync(id);
        }

        [AllowAnonymous]
        public override Task<PagedResultDto<CountryDto>> GetListAsync(CountryListRequestDto input)
        {
            return base.GetListAsync(input);
        }

        public override Task<CountryDto> CreateAsync(CountryDto input)
        {
            throw new ApplicationException("Creating countries is not supported.");
        }

        public override Task DeleteAsync(Guid id)
        {
            throw new ApplicationException("Deleting countries is not support. Set the country inactive instead.");
        }

        protected override IQueryable<Country> CreateFilteredQuery(CountryListRequestDto input)
        {
            var filteredQuery = input.ShowInactive ? Repository.Where(country => !country.Active) : Repository.Where(country => country.Active);

            input.Keyword = input.Keyword?.Trim();

            if (string.IsNullOrWhiteSpace(input.Keyword))
            {
                return filteredQuery;
            }

            return filteredQuery.Where(country => 
                country.Name.StartsWith(input.Keyword, StringComparison.InvariantCultureIgnoreCase)
                || country.AlphaCode.Equals(input.Keyword, StringComparison.CurrentCultureIgnoreCase)
            );
        }
    }
}