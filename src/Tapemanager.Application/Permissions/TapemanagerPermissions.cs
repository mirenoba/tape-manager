﻿using Volo.Abp.Reflection;

namespace Tapemanager.Permissions
{
    public static class TapemanagerPermissions
    {
        public const string GroupName = "Tapemanager";

        public static class Countries
        {
            public const string Default = GroupName + ".Countries";
            public const string List = Default + ".List";
            public const string Update = Default + ".Update";
        }

        public static class Locations
        {
            public const string Default = GroupName + ".Locations";
            public const string List = Default + ".List";
            public const string Create = Default + ".Create";
            public const string Update = Default + ".Update";
            public const string Delete = Default + ".Delete";
            public const string DeletePermanently = Default + ".DeletePermanently";
        }

        public static string[] GetAll()
        {
            return ReflectionHelper.GetPublicConstantsRecursively(typeof(TapemanagerPermissions));
        }
    }
}