﻿using Tapemanager.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;
using Volo.Abp.MultiTenancy;

namespace Tapemanager.Permissions
{
    public class TapemanagerPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(TapemanagerPermissions.GroupName);

            #region Country Permissions
            var countries = myGroup.AddPermission(TapemanagerPermissions.Countries.Default,
                L($"Permission:{TapemanagerPermissions.Countries.Default}"),
                MultiTenancySides.Host);
            countries.AddChild(TapemanagerPermissions.Countries.List,
                L($"Permission:{TapemanagerPermissions.Countries.List}"),
                MultiTenancySides.Host);
            countries.AddChild(TapemanagerPermissions.Countries.Update,
                L($"Permission:{TapemanagerPermissions.Countries.Update}"),
                MultiTenancySides.Host);
            #endregion

            #region Location Permissions

            var locations = myGroup.AddPermission(TapemanagerPermissions.Locations.Default,
                L($"Permission:{TapemanagerPermissions.Locations.Default}"), MultiTenancySides.Tenant);

            locations.AddChild(TapemanagerPermissions.Locations.List,
                L($"Permission:{TapemanagerPermissions.Locations.List}"), MultiTenancySides.Tenant);
            locations.AddChild(TapemanagerPermissions.Locations.Create,
                L($"Permission:{TapemanagerPermissions.Locations.Create}"), MultiTenancySides.Tenant);
            locations.AddChild(TapemanagerPermissions.Locations.Update,
                L($"Permission:{TapemanagerPermissions.Locations.Update}"), MultiTenancySides.Tenant);
            locations.AddChild(TapemanagerPermissions.Locations.Delete,
                L($"Permission:{TapemanagerPermissions.Locations.Delete}"), MultiTenancySides.Tenant);
            locations.AddChild(TapemanagerPermissions.Locations.DeletePermanently,
                L($"Permission:{TapemanagerPermissions.Locations.DeletePermanently}"), MultiTenancySides.Tenant);

            #endregion
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<TapemanagerResource>(name);
        }
    }
}
