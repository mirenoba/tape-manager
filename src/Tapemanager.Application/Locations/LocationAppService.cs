﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Tapemanager.Countries;
using Tapemanager.Features;
using Tapemanager.Permissions;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Features;
using Volo.Abp.MultiTenancy;

namespace Tapemanager.Locations
{
    [Authorize(TapemanagerPermissions.Locations.Default)]
    [RequiresFeature(TapemanagerFeatures.Locations.Default)]
    public class LocationAppService : AsyncCrudAppService<Location, LocationDto, Guid, PagedAndSortedResultRequestDto, CreateLocationDto, CreateLocationDto>, ILocationAppService
    {
        #region Permission Names
        protected override string GetPolicyName => TapemanagerPermissions.Locations.List;
        protected override string GetListPolicyName => TapemanagerPermissions.Locations.List;
        protected override string CreatePolicyName => TapemanagerPermissions.Locations.Create;
        protected override string UpdatePolicyName => TapemanagerPermissions.Locations.Update;
        protected override string DeletePolicyName => TapemanagerPermissions.Locations.Delete; 
        #endregion

        public IRepository<Country> Countries { get; }

        public ILocationPolicy LocationPolicy { get; }

        public LocationAppService(IRepository<Location, Guid> repository, IRepository<Country> countries, ILocationPolicy locationPolicy) : base(repository)
        {
            Countries = countries;
            LocationPolicy = locationPolicy;
        }

        public override async Task<LocationDto> CreateAsync(CreateLocationDto input)
        {
            await CheckCreatePolicyAsync();

            var entity = MapToEntity(input);

            TryToSetTenantId(entity);

            await LocationPolicy.CheckCreationAsync(entity);

            await Repository.InsertAsync(entity, autoSave: true);

            return MapToGetOutputDto(entity);
        }

        protected override LocationDto MapToGetOutputDto(Location entity)
        {
            var locationDto = base.MapToGetOutputDto(entity);

            if (locationDto.Address?.CountryId == null || locationDto.Address.CountryId.Equals(Guid.Empty))
            {
                return locationDto;
            }

            var countryDto = ObjectMapper.Map<Country, CountryDto>(Countries.SingleOrDefault(country => country.Id.Equals(locationDto.Address.CountryId)));
            locationDto.Address.Country = countryDto;

            return locationDto;
        }
    }
}