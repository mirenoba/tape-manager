﻿using AutoMapper;
using Tapemanager.Countries;
using Tapemanager.Locations;

namespace Tapemanager
{
    public class TapemanagerApplicationAutoMapperProfile : Profile
    {
        public TapemanagerApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */

            #region Country

            CreateMap<Country, CountryDto>().ReverseMap();
            CreateMap<UpdateCountryDto, Country>().ReverseMap();

            #endregion

            #region Location

            CreateMap<Location, LocationDto>().ReverseMap();
            CreateMap<CreateLocationDto, Location>().ReverseMap();

            CreateMap<Address, AddressDto>().ReverseMap();
            CreateMap<CreateAddressDto, Address>().ReverseMap();

            #endregion
        }
    }
}
