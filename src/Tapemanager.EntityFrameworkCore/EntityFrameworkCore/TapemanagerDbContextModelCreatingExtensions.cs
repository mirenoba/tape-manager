﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tapemanager.Countries;
using Tapemanager.Locations;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;
using Volo.Abp.Users;

namespace Tapemanager.EntityFrameworkCore
{
    public static class TapemanagerDbContextModelCreatingExtensions
    {
        public static void ConfigureTapemanager(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            /* Configure your own tables/entities inside here */
            builder.Entity<Country>(b =>
            {
                b.ToTable(TapemanagerConsts.DbTablePrefix + "Countries", TapemanagerConsts.DbSchema);
                b.ConfigureByConvention();
                b.HasIndex(country => country.Name);
            });

            builder.Entity<Location>(b =>
            {
                b.ToTable(TapemanagerConsts.DbTablePrefix + "Locations", TapemanagerConsts.DbSchema);
                b.ConfigureByConvention();

                b.Property(location => location.Name).HasMaxLength(128);
                b.HasIndex(location => location.Name).IsUnique();

                // owned types can only be null if mapped to a separate table
                b.OwnsOne(location => location.Address, ob =>
                {
                    ob.ToTable(TapemanagerConsts.DbTablePrefix + "Locations_Addresses", TapemanagerConsts.DbSchema);
                    ob.HasOne(a => a.Country).WithOne();
                });
            });
        }

        public static void ConfigureCustomUserProperties<TUser>(this EntityTypeBuilder<TUser> b)
            where TUser: class, IUser
        {
            //b.Property<string>(nameof(AppUser.MyProperty))...
        }
    }
}