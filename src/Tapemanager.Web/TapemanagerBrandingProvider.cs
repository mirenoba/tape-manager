﻿using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared.Components;
using Volo.Abp.DependencyInjection;

namespace Tapemanager.Web
{
    [Dependency(ReplaceServices = true)]
    public class TapemanagerBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "Tapemanager";
    }
}
