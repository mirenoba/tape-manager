﻿using Tapemanager.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Tapemanager.Web.Pages
{
    /* Inherit your PageModel classes from this class.
     */
    public abstract class TapemanagerPageModel : AbpPageModel
    {
        protected TapemanagerPageModel()
        {
            LocalizationResourceType = typeof(TapemanagerResource);
        }
    }
}