﻿using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Tapemanager.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Tapemanager.Web.Pages
{
    /* Inherit your UI Pages from this class. To do that, add this line to your Pages (.cshtml files under the Page folder):
     * @inherits Tapemanager.Web.Pages.TapemanagerPage
     */
    public abstract class TapemanagerPage : AbpPage
    {
        [RazorInject]
        public IHtmlLocalizer<TapemanagerResource> L { get; set; }
    }
}
