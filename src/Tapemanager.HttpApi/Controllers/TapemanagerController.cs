﻿using Tapemanager.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace Tapemanager.Controllers
{
    /* Inherit your controllers from this class.
     */
    public abstract class TapemanagerController : AbpController
    {
        protected TapemanagerController()
        {
            LocalizationResource = typeof(TapemanagerResource);
        }
    }
}