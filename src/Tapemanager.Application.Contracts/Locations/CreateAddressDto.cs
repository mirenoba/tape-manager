﻿using System;

namespace Tapemanager.Locations
{
    public class CreateAddressDto
    {
        public string Street { get; set; }

        public string AdditionalInformation { get; set; }

        public string StreetNumber { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public Guid CountryId { get; set; }
    }
}