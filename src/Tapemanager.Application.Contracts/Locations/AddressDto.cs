﻿using System;
using System.Text.RegularExpressions;
using Tapemanager.Countries;

namespace Tapemanager.Locations
{
    public class AddressDto
    {
        public string Street { get; set; }

        public string AdditionalInformation { get; set; }

        public string StreetNumber { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public Guid CountryId { get; set; }

        public CountryDto Country { get; set; }

        public string FormattedAddress => Regex.Replace($"{Street} {StreetNumber}, {PostalCode} {City}, {Country}", @"\s+", " ");
    }
}