﻿using System.ComponentModel.DataAnnotations;

namespace Tapemanager.Locations
{
    public class CreateLocationDto
    {
        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        public CreateAddressDto Address { get; set; }
    }
}