﻿using System;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Tapemanager.Locations
{
    public interface ILocationAppService : IAsyncCrudAppService<LocationDto, Guid, PagedAndSortedResultRequestDto, CreateLocationDto, CreateLocationDto>
    {
        
    }
}