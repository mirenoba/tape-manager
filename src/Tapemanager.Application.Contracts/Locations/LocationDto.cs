﻿using System;
using Volo.Abp.Application.Dtos;

namespace Tapemanager.Locations
{
    public class LocationDto : FullAuditedEntityDto<Guid>
    {
        public string Name { get; set; }

        public virtual AddressDto Address { get; set; }
    }
}