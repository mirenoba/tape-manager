﻿using System;
using Volo.Abp.Application.Dtos;

namespace Tapemanager.Countries
{
    public class UpdateCountryDto
    {
        /// <summary>
        /// Determines if this country can be seen in any lists.
        /// </summary>
        public bool Active { get; set; }
    }
}