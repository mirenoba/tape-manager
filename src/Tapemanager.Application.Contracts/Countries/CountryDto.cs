﻿using System;
using Volo.Abp.Application.Dtos;

namespace Tapemanager.Countries
{
    public class CountryDto : EntityDto<Guid>
    {
        public string Name { get; set; }

        /// <summary>
        /// ISO 3166-1 2 Letter Code, this is not unique
        /// </summary>
        public string AlphaCode { get; set; }

        /// <summary>
        /// ISO-3166-1 numeric code, this is not unique
        /// </summary>
        public short NumericCode { get; set; }

        /// <summary>
        /// Determines if this country can be seen in any lists.
        /// </summary>
        public bool Active { get; set; }
    }
}