﻿using Volo.Abp.Application.Dtos;

namespace Tapemanager.Countries
{
    public class CountryListRequestDto : PagedAndSortedResultRequestDto
    {
        /// <summary>
        /// Set to true if you want to list all countries.
        /// This is useful to toggle inactive countries into the active state.
        /// </summary>
        public bool ShowInactive { get; set; } = false;

        /// <summary>
        /// Allows filtering the country list by their name and country codes.
        /// </summary>
        public string Keyword { get; set; }
    }
}