﻿using System;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Tapemanager.Countries
{
    public interface ICountryAppService : IAsyncCrudAppService<CountryDto, Guid, CountryListRequestDto, CountryDto, UpdateCountryDto>
    {
        
    }
}