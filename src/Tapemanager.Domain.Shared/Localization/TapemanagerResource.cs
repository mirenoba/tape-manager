﻿using Volo.Abp.Localization;

namespace Tapemanager.Localization
{
    [LocalizationResourceName("Tapemanager")]
    public class TapemanagerResource
    {

    }
}