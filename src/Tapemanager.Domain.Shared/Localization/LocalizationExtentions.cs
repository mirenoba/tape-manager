﻿using Volo.Abp.Localization;

namespace Tapemanager.Localization
{
    public static class LocalizationExtentions
    {
        public static LocalizableString L(this string name)
        {
            return LocalizableString.Create<TapemanagerResource>(name);
        }
    }
}