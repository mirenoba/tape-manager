﻿namespace Tapemanager
{
    public static class TapemanagerDomainErrorCodes
    {
        /* You can add your business exception error codes here, as constants */
        public const string FeatureNotEnabled = "0x0F01";
        public const string FeatureLimitReached = "0x0F02";
    }
}
