﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Tapemanager.Data;
using Volo.Abp.DependencyInjection;

namespace Tapemanager.EntityFrameworkCore
{
    [Dependency(ReplaceServices = true)]
    public class EntityFrameworkCoreTapemanagerDbSchemaMigrator 
        : ITapemanagerDbSchemaMigrator, ITransientDependency
    {
        private readonly TapemanagerMigrationsDbContext _dbContext;

        public EntityFrameworkCoreTapemanagerDbSchemaMigrator(TapemanagerMigrationsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task MigrateAsync()
        {
            await _dbContext.Database.MigrateAsync();
        }
    }
}