﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace Tapemanager.EntityFrameworkCore
{
    [DependsOn(
        typeof(TapemanagerEntityFrameworkCoreModule)
        )]
    public class TapemanagerEntityFrameworkCoreDbMigrationsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<TapemanagerMigrationsDbContext>();
        }
    }
}
