﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Tapemanager.Migrations
{
    public partial class Add_CountryId_Index_To_Address : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_AppLocations_Addresses_CountryId",
                table: "AppLocations_Addresses");

            migrationBuilder.CreateIndex(
                name: "IX_AppLocations_Addresses_CountryId",
                table: "AppLocations_Addresses",
                column: "CountryId",
                unique: true,
                filter: "[CountryId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_AppLocations_Addresses_CountryId",
                table: "AppLocations_Addresses");

            migrationBuilder.CreateIndex(
                name: "IX_AppLocations_Addresses_CountryId",
                table: "AppLocations_Addresses",
                column: "CountryId");
        }
    }
}
