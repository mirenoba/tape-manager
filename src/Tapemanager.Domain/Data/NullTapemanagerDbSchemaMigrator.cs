﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace Tapemanager.Data
{
    /* This is used if database provider does't define
     * ITapemanagerDbSchemaMigrator implementation.
     */
    public class NullTapemanagerDbSchemaMigrator : ITapemanagerDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}