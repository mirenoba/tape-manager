﻿using System.Threading.Tasks;

namespace Tapemanager.Data
{
    public interface ITapemanagerDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
