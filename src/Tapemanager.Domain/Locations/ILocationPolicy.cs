﻿using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace Tapemanager.Locations
{
    public interface ILocationPolicy : IDomainService
    {
        Task CheckCreationAsync(Location location);
    }
}