﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;
using Tapemanager.Countries;
using Volo.Abp.Domain.Values;

namespace Tapemanager.Locations
{
    public class Address : ValueObject
    {
        public string Street { get; set; }

        public string AdditionalInformation { get; set; }

        public string StreetNumber { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public Guid? CountryId { get; set; }

        public virtual Country Country { get; set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Street;
            yield return AdditionalInformation;
            yield return StreetNumber;
            yield return PostalCode;
            yield return City;
            yield return State;
        }
    }
}