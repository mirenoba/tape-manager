﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Tapemanager.Features;
using Volo.Abp;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Services;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Features;

namespace Tapemanager.Locations
{
    public class LocationPolicy : DomainService, ILocationPolicy
    {
        public LocationPolicy(IRepository<Location> locations, IFeatureChecker featureChecker)
        {
            Locations = locations;
            FeatureChecker = featureChecker;
        }

        public IRepository<Location> Locations { get; }

        public IFeatureChecker FeatureChecker { get; }

        public async Task CheckCreationAsync(Location location)
        {
            var locationCount = await Locations.GetCountAsync();

            if (!await FeatureChecker.IsEnabledAsync(TapemanagerFeatures.Locations.Default))
            {
                throw new BusinessException(TapemanagerDomainErrorCodes.FeatureNotEnabled, 
                    $"Feature not enabled", 
                    $"The feature {TapemanagerFeatures.Locations.Default} must be enabled to allow the creation of new locations.");
            }

            var maxLocations = (await FeatureChecker.GetAsync(TapemanagerFeatures.Locations.MaxLocations, TapemanagerFeatures.Locations.MaxLocationsDefault)).To<int>();

            if (locationCount >= maxLocations)
            {
                throw new UserFriendlyException(
                    "You reached your location limit.", 
                    TapemanagerDomainErrorCodes.FeatureLimitReached,
                    $"You can only create a maximum of {maxLocations} locations. Please upgrade your subscription plan or delete existing locations.",
                    logLevel: LogLevel.Information
                );
            }
        }
    }
}