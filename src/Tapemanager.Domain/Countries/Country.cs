﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities;

namespace Tapemanager.Countries
{
    public class Country : Entity<Guid>
    {
        public string Name { get; set; }

        /// <summary>
        /// ISO 3166-1 2 Letter Code, this is not unique
        /// </summary>
        public string AlphaCode { get; set; }

        /// <summary>
        /// ISO-3166-1 numeric code, this is not unique
        /// </summary>
        public short NumericCode { get; set; }

        /// <summary>
        /// Determines if this country can be seen in any lists.
        /// </summary>
        public bool Active { get; set; }

        [NotMapped]
        public bool Inactive => !Active;

        public override string ToString() => $"{Name}";
    }
}