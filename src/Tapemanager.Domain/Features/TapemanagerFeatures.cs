﻿namespace Tapemanager.Features
{
    public static class TapemanagerFeatures
    {
        public const string Root = "Tapemanager";

        public static class Locations
        {
            public const string Default = Root +  ":Locations";

            public const string MaxLocations = Default + ":MaxLocations";
            public const int MaxLocationsDefault = 2;
        }
    }
}