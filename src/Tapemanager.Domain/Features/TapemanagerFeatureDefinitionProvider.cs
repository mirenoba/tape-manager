﻿using Tapemanager.Localization;
using Volo.Abp.Features;
using Volo.Abp.Localization;
using Volo.Abp.Validation.StringValues;

namespace Tapemanager.Features
{
    public class TapemanagerFeatureDefinitionProvider : FeatureDefinitionProvider
    {
        public override void Define(IFeatureDefinitionContext context)
        {
            var root = context.AddGroup(TapemanagerFeatures.Root, $"Features:{TapemanagerFeatures.Root}".L());

            var locationsFeature = root.AddFeature(TapemanagerFeatures.Locations.Default, true.ToString(),
                $"Features:{TapemanagerFeatures.Locations.Default}".L(),
                $"Features:{TapemanagerFeatures.Locations.Default}_Description".L(),
                new ToggleStringValueType());

            locationsFeature.CreateChild(
                TapemanagerFeatures.Locations.MaxLocations,
                TapemanagerFeatures.Locations.MaxLocationsDefault.ToString(),
                $"Features:{TapemanagerFeatures.Locations.MaxLocations}".L(),
                $"Features:{TapemanagerFeatures.Locations.MaxLocations}_Description".L(),
                new FreeTextStringValueType(new NumericValueValidator(1))
            );
        }
    }
}