﻿using Volo.Abp.Settings;

namespace Tapemanager.Settings
{
    public class TapemanagerSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(TapemanagerSettings.MySetting1));
        }
    }
}
