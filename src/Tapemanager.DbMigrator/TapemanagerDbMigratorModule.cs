﻿using Tapemanager.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;

namespace Tapemanager.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(TapemanagerEntityFrameworkCoreDbMigrationsModule),
        typeof(TapemanagerApplicationContractsModule)
        )]
    public class TapemanagerDbMigratorModule : AbpModule
    {
        
    }
}
