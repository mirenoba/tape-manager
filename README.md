# Tapemanager

This project was created using the [ABP (ASP.NET Boilerplate vNext)](https://abp.io) framework.

## Getting started

To get started you should take a quick look at the [ABP guide](https://docs.abp.io/en/abp/latest/Tutorials/AspNetCore-Mvc/Part-I). This is how the `Tapemanager` project looks.

![Project Structure](docs/img/Tapemanager_Project_Structure.PNG)

* Create [`Entity`](https://docs.abp.io/en/abp/latest/Entities) objects inside the `Domain` project.
* Create shared `Enums`, `Constants`, etc. inside the `Domain.Shared` project.
* Create your DTOs ([Data Transfer Objects](https://docs.abp.io/en/abp/latest/Data-Transfer-Objects)) inside the `Application.Contracts` project.
* Create your [`Application Services`](https://docs.abp.io/en/abp/latest/Application-Services) inside the `Application` project.
* `API Controllers` will be automagically created from your `Application Services`.

### First launch

Before you can launch the project, you need to create your local database. Simply select the `Tapemanager.DbMigrator` console application and start it. This will create your local database, apply migrations and fill it with seed data.

After this you are ready to launch the `Tapemanager.Web` application and login with:

* **Username**: `admin`
* **Password**: `1q2w3E*`
