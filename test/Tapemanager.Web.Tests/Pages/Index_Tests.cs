﻿using System.Threading.Tasks;
using Shouldly;
using Xunit;

namespace Tapemanager.Pages
{
    public class Index_Tests : TapemanagerWebTestBase
    {
        [Fact]
        public async Task Welcome_Page()
        {
            var response = await GetResponseAsStringAsync("/");
            response.ShouldNotBeNull();
        }
    }
}
