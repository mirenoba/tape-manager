﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace Tapemanager.HttpApi.Client.ConsoleTestApp
{
    [DependsOn(
        typeof(TapemanagerHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class TapemanagerConsoleApiClientModule : AbpModule
    {
        
    }
}
