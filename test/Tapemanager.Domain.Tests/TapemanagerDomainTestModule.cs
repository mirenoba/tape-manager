﻿using Tapemanager.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace Tapemanager
{
    [DependsOn(
        typeof(TapemanagerEntityFrameworkCoreTestModule)
        )]
    public class TapemanagerDomainTestModule : AbpModule
    {

    }
}