﻿using Microsoft.Extensions.DependencyInjection;
using Tapemanager.TestData;
using Volo.Abp;
using Volo.Abp.Modularity;

namespace Tapemanager
{
    [DependsOn(
        typeof(TapemanagerApplicationModule),
        typeof(TapemanagerDomainTestModule)
        )]
    public class TapemanagerApplicationTestModule : AbpModule
    {
        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            SeedTestData(context);
        }

        private static void SeedTestData(ApplicationInitializationContext context)
        {
            //using (var scope = context.ServiceProvider.CreateScope())
            //{
            //    scope.ServiceProvider
            //        .GetRequiredService<LocationTestDataBuilder>()
            //        .Build();
            //}
        }
    }
}