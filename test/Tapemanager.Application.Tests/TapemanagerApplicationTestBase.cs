﻿using Xunit;

namespace Tapemanager
{
    [Trait("Category", "Functional")]
    public abstract class TapemanagerApplicationTestBase : TapemanagerTestBase<TapemanagerApplicationTestModule> 
    {

    }
}
