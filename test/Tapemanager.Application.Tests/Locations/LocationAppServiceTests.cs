﻿using System;
using AutoBogus;
using Bogus;
using Microsoft.EntityFrameworkCore;
using Shouldly;
using Tapemanager.Countries.Data;
using Tapemanager.Features;
using Tapemanager.TestData;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;
using Xunit;

namespace Tapemanager.Locations
{
    public class LocationAppServiceTests : TapemanagerApplicationTestBase
    {
        public LocationAppServiceTests()
        {
            Repository = GetRequiredService<IRepository<Location, Guid>>();
            Service = GetRequiredService<ILocationAppService>();
            GetRequiredService<LocationTestDataBuilder>().Build();
        }

        public ILocationAppService Service { get; }

        public IRepository<Location, Guid> Repository { get; }

        private CreateLocationDto GenerateCreateLocationDto()
        {
            return new CreateLocationDto
            {
                Name = Faker.Name.FullName(),
                Address = new CreateAddressDto
                {
                    Street = Faker.Address.StreetName(),
                    StreetNumber = Faker.Address.BuildingNumber(),
                    State = Faker.Address.State(),
                    AdditionalInformation = Faker.Address.SecondaryAddress(),
                    City = Faker.Address.City(),
                    PostalCode = Faker.Address.ZipCode(),
                    CountryId = CountryDataSeedContributor
                        .Countries[RandomHelper.GetRandom(CountryDataSeedContributor.Countries.Count)].Id
                }
            };
        }

        [Fact(DisplayName = "Create Location should create location with address")]
        public async void Test_Create_CreatesLocation_WithAddress()
        {
            var result = await Service.CreateAsync(GenerateCreateLocationDto());

            result.ShouldNotBeNull();
            result.Address.ShouldNotBeNull();
            result.Address.Country.ShouldNotBeNull();

            await WithUnitOfWorkAsync(async () =>
            {
                var entity = await Repository.Include(location => location.Address.Country)
                    .FirstAsync(location => location.Id.Equals(result.Id));

                entity.ShouldNotBeNull();
                entity.Name.ShouldBe(result.Name);
                entity.Address.ShouldNotBeNull();
                entity.Address.Street.ShouldBe(result.Address.Street);
                entity.Address.Country.ShouldNotBeNull();
                entity.Address.CountryId.ShouldBe(result.Address.Country.Id);
                entity.Address.Country.Name.ShouldBe(result.Address.Country.Name);
            });
        }

        [Fact(DisplayName = "Create should check the max locations feature limit")]
        public async void Test_Create_Checks_FeatureLimit()
        {
            var maxLocationsCount = TapemanagerFeatures.Locations.MaxLocationsDefault - LocationTestDataBuilder.LocationCount;

            for (int i = 0; i < maxLocationsCount; i++)
            {
                await Service.CreateAsync(GenerateCreateLocationDto());
            }

            // the next location should overreach the limit
            await Service.CreateAsync(GenerateCreateLocationDto())
                .ShouldThrowAsync<UserFriendlyException>();
        }

        [Fact(DisplayName = "GetList should return a list of seed locations")]
        public async void Test_GetList_Returns_Seeded_Locations()
        {
            var result = await Service.GetListAsync(new PagedAndSortedResultRequestDto());

            result.TotalCount.ShouldBe(LocationTestDataBuilder.LocationCount);
        }

        [Fact(DisplayName = "GetList should return a list of locations including their address and country")]
        public async void Test_GetList_Returns_Locations_WithAddress()
        {
            var result = await Service.GetListAsync(new PagedAndSortedResultRequestDto());

            result.TotalCount.ShouldBe(LocationTestDataBuilder.LocationCount);
            result.Items.ShouldAllBe(dto => dto.Address != null);
            result.Items.ShouldAllBe(dto => dto.Address.Country != null);
        }
    }
}