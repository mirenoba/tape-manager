﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Shouldly;
using Tapemanager.Countries.Data;
using Volo.Abp.Domain.Repositories;
using Xunit;

namespace Tapemanager.Countries
{
    public class CountryAppServiceTests : TapemanagerApplicationTestBase
    {
        private readonly ICountryAppService _service;
        private readonly IRepository<Country> _countryRepository;

        public CountryAppServiceTests()
        {
            _service = GetRequiredService<ICountryAppService>();
            _countryRepository = GetRequiredService<IRepository<Country>>();
        }

        [Fact(DisplayName = "GetList should only returns active countries by default")]
        public async void Test_GetList_Returns_OnlyActive_Countries()
        {
            var result = await _service.GetListAsync(new CountryListRequestDto());

            result.TotalCount.ShouldBe(CountryDataSeedContributor.Countries.Count(country => country.Active));
            result.Items.ShouldNotContain(dto => !dto.Active);
        }

        [Fact(DisplayName = "GetList should return a list of seeded countries")]
        public async void Test_GetList_Returns_SeededCountries()
        {
            var result = await _service.GetListAsync(new CountryListRequestDto());

            result.TotalCount.ShouldBeGreaterThan(0);
        }

        [Fact(DisplayName = "GetList ShowInactive = true should return only inactive countries")]
        public async void Test_GetList_ShowInactive_Returns_OnlyInactive_Countries()
        {
            var result = await _service.GetListAsync(new CountryListRequestDto {ShowInactive = true});

            result.TotalCount.ShouldBe(CountryDataSeedContributor.Countries.Count(country => !country.Active));
            result.Items.ShouldNotContain(dto => dto.Active);
        }

        [Fact(DisplayName = "GetList request with keyword should filter countries by their name")]
        public async void Test_GetList_Filters_ByName()
        {
            var keyword = "united";
            var expected = CountryDataSeedContributor.Countries.Where(country =>
                country.Name.StartsWith(keyword, StringComparison.InvariantCultureIgnoreCase));

            var result = await _service.GetListAsync(new CountryListRequestDto
            {
                ShowInactive = true,
                Keyword = keyword
            });

            result.TotalCount.ShouldBe(expected.Count());
            result.Items.ShouldContain(dto => dto.Name.Equals("United States"));
        }

        [Fact(DisplayName = "GetList request with keyword should filter countries by their alpha country code")]
        public async void Test_GetList_Should_Filter_ByCountryCode()
        {
            var keyword = "gb";

            var result = await _service.GetListAsync(new CountryListRequestDto
            {
                ShowInactive = true,
                Keyword = keyword
            });

            result.TotalCount.ShouldBe(1);
            result.Items.ShouldContain(dto => dto.Name.Equals("United Kingdom"));
        }

        [Fact(DisplayName = "GetList with keyword should trim whitespaces")]
        public async void Test_GetList_Trims_Keyword()
        {
            var keyword = "     german   ";

            var result = await _service.GetListAsync(new CountryListRequestDto
            {
                Keyword = keyword
            });

            result.TotalCount.ShouldBe(1);
            result.Items.ShouldContain(dto => dto.Name.Equals("Germany"));
        }

        [Fact(DisplayName = "Update should change the active state of the country")]
        public async void Test_Update_Changes_Active_State()
        {
            var country = CountryDataSeedContributor.Countries.First(c => c.Inactive);

            await _service.UpdateAsync(country.Id, new UpdateCountryDto {Active = true});

            await WithUnitOfWorkAsync(async () =>
            {
                var result = await _countryRepository.FirstAsync(c => c.Id.Equals(country.Id));
                result.Active.ShouldBeTrue();
            });
        }

        [Fact(DisplayName = "Create should not be allowed and throws an ApplicationException")]
        public async void Test_Create_Throws_ApplicationException()
        {
            await _service.CreateAsync(new CountryDto()).ShouldThrowAsync<ApplicationException>();
        }

        [Fact(DisplayName = "Delete should not be allowed and throws an ApplicationException")]
        public async void Test_Delete_Throws_ApplicationException()
        {
            await _service.DeleteAsync(Guid.NewGuid()).ShouldThrowAsync<ApplicationException>();
        }
    }
}