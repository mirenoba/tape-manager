﻿using System;
using System.Threading.Tasks;
using AutoBogus;
using Bogus;
using Tapemanager.Countries.Data;
using Tapemanager.Locations;
using Volo.Abp;
using Volo.Abp.Auditing;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.MultiTenancy;
using Volo.Abp.Threading;
using Volo.Abp.Uow;

namespace Tapemanager.TestData
{
    public class LocationTestDataBuilder : ITransientDependency
    {
        public const int LocationCount = 1;

        public LocationTestDataBuilder(IRepository<Location> locations)
        {
            Locations = locations;

            AutoFaker.Configure(builder =>
            {
                builder.WithSkip<Location>(o => o.CreationTime);
                builder.WithSkip<Location>(o => o.CreatorId);
                builder.WithSkip<Location>(o => o.DeleterId);
                builder.WithSkip<Location>(o => o.DeletionTime);
                builder.WithSkip<Location>(o => o.IsDeleted);
                builder.WithSkip<Location>(o => o.LastModificationTime);
                builder.WithSkip<Location>(o => o.LastModifierId);

                builder.WithSkip<Location>(entity => entity.Id);

                builder.WithSkip<Location>(tenant => tenant.TenantId);

                builder.WithSkip<Address>(address => address.Country);

                builder.WithOverride<Address>(context =>
                {
                    var address = context.Instance as Address;
                    address.CountryId = CountryDataSeedContributor.Countries[RandomHelper.GetRandom(CountryDataSeedContributor.Countries.Count)].Id;

                    return address;
                });
            });
        }

        public IRepository<Location> Locations { get; }

        public void Build()
        {
            AddLocationAsync();
        }

        private void AddLocationAsync()
        {
            foreach (var location in AutoFaker.Generate<Location>(LocationCount))
            {
                Locations.Insert(location, true);
            }
        }
    }
}